<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Entity\Category;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /*
         *Category 1
         */

        $category1= new Category();
        $category1-> setLabel('Intelligence émotionnelle');

        $manager->persist($category1);
        $manager->flush();

        /*
         *Category 2
         */

        $category2= new Category();
        $category2->setLabel('Monde professionnel');

        $manager->persist($category2);
        $manager->flush();

        /*
         *Category 3
         */

        $category3= new Category();
        $category3->setLabel('Qualité de vie');

        $manager->persist($category3);
        $manager->flush();

        /*
         *Category 4
         */

        $category4= new Category();
        $category4->setLabel('Développement personnel');

        $manager->persist($category4);
        $manager->flush();
        /*
         *Category 5
         */

        $category5= new Category();
        $category5->setLabel('Autre');

        $manager->persist($category5);
        $manager->flush();

        /*
         *Category 6
         */

        $category6= new Category();
        $category6->setLabel('Autre');

        $manager->persist($category6);
        $manager->flush();

        /*
         *Category 7
         */

        $category7= new Category();
        $category7->setLabel('Autre');

        $manager->persist($category7);
        $manager->flush();

        /*
         *Category 8
         */

        $category8= new Category();
        $category8->setLabel('Autre');

        $manager->persist($category8);
        $manager->flush();

        /*
         *Category 9
         */

        $category9= new Category();
        $category9->setLabel('Autre');

        $manager->persist($category9);
        $manager->flush();

        /*
         *Category 10
         */

        $category10= new Category();
        $category10->setLabel('Autre');

        $manager->persist($category10);
        $manager->flush();

        /*
         *Category 11
         */

        $category11= new Category();
        $category11->setLabel('Vidéo');

        $manager->persist($category11);
        $manager->flush();

        /*
        *Category 12
        */

        $category12= new Category();
        $category12->setLabel('Défi');

        $manager->persist($category12);
        $manager->flush();

        /*
         *Category 13
         */

        $category13= new Category();
        $category13->setLabel('Livre');

        $manager->persist($category13);
        $manager->flush();

    }
    public function getOrder(): int
    {
        return 1;
    }
}