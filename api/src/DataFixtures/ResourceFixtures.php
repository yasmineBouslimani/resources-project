<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\ResourceType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Entity\Resource;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ResourceFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /*
         *Resource 1
         */

        $resource1= new Resource();
        $resourceType1= new ResourceType();
        $resourceType1->setLabel('Vidéo');
        $category1= new Category();
        $category1-> setLabel('Intelligence émotionnelle');
        $resource1->setType($resourceType1);
        $resource1->setCategory($category1);
        $resource1->setName('1962 : l\'An 2000 vu par les jeunes | Archive INA');
        $resource1->setDescription('Vidéo d\'archive de l\'Institut National de l\'Audiovisuel réalisée le 3 octobre 1962.');
        $resource1->setImage('https://i.ytimg.com/vi/kAMQZ5N49WU/maxresdefault.jpg');
        $resource1->setActivatedAt(new \DateTimeImmutable('2022-01-01 00:00:00'));

        $manager->persist($resource1);
        $manager->flush();

        /*
         *Resource 2
         */

        $resource2= new Resource();
        $resourceType2= new ResourceType();
        $resourceType2->setLabel('Jeu');
        $category2= new Category();
        $category2->setLabel('Monde professionnel');
        $resource2->setType($resourceType2);
        $resource2->setCategory($category2);
        $resource2->setName('Les Innovateurs - Walter ISAACSON');
        $resource2->setDescription('L’explosion numérique n’est pas seulement le fait de quelques génies, c’est une entreprise collective, le résultat d’un travail d’équipes qui ont avancé ensemble, bravant les obstacles les uns après les autres.');
        $resource2->setImage('https://www.sowhat-blog.fr/wp-content/uploads/2017/05/Image-%C3%A0-la-une-Les-innovateurs-%E2%80%93-Walter-Isaacson.jpg');
        $resource2->setActivatedAt(new \DateTimeImmutable('2022-01-01 00:00:00'));

        $manager->persist($resource2);
        $manager->flush();

        /*
         *Resource 3
         */

        $resource3= new Resource();
        $resourceType3= new ResourceType();
        $resourceType3->setLabel('Livre');
        $category3= new Category();
        $category3->setLabel('Qualité de vie');
        $resource3->setType($resourceType3);
        $resource3->setCategory($category3);
        $resource3->setName('Metting de Melenchon du 16 janvier 2022');
        $resource3->setDescription('Jean-Luc Mélenchon organise dimanche 16 janvier à Nantes un meeting \"immersif\". Après les hologrammes, le candidat de la France Insoumise propose donc un dispositif inédit. Une fois encore La France insoumise veut créer l\'événement.');
        $resource3->setImage('https://www.francetvinfo.fr/pictures/2THik5rrCwp6k9K8vEYs3-SDi3M/752x423/2022/01/17/phpB8GkFT.jpg');
        $resource3->setActivatedAt(new \DateTimeImmutable('2022-01-01 00:00:00'));

        $manager->persist($resource3);
        $manager->flush();

        /*
         *Resource 4
         */

        $resource4= new Resource();
        $resourceType4= new ResourceType();
        $resourceType4->setLabel('Conférence');
        $category4= new Category();
        $category4->setLabel('Développement personnel');
        $resource4->setType($resourceType4);
        $resource4->setCategory($category4);
        $resource4->setName('Jeux vidéo CyberPunk 2077');
        $resource4->setDescription('Cyberpunk 2077 est un jeu vidéo d\'action-RPG en vue à la première personne développé par CD Projekt RED, inspiré du jeu de rôle sur table Cyberpunk 2020 conçu par Mike Pondsmith.');
        $resource4->setImage('https://cdn1.epicgames.com/offer/77f2b98e2cef40c8a7437518bf420e47/2c1e97ed-3ef5-4b15-928a-20a10a2197ba_2560x1440-5294ae9f2b864121a81e56518ccd3a42');
        $resource4->setActivatedAt(new \DateTimeImmutable('2022-01-01 00:00:00'));

        $manager->persist($resource4);
        $manager->flush();

        /*
         *Resource 5
         */

        $resource5= new Resource();
        $resourceType5= new ResourceType();
        $resourceType5->setLabel('Défi');
        $category5= new Category();
        $category5->setLabel('Autre');
        $resource5->setType($resourceType5);
        $resource5->setCategory($category5);
        $resource5->setName('JO 2022 : la Française Chloé Trespeuch médaillée d’argent en snowboardcross');
        $resource5->setDescription('Après le bronze aux Jeux olympiques de 2014 et une frustrante cinquième place en 2018, la snowboardeuse de 27 ans a pris la 2e place dans l’épreuve de cross.');
        $resource5->setImage('https://img.lemde.fr/2022/02/09/619/0/7431/3715/1600/800/60/0/957cb82_533bd4ccd1d84e88af7edf64ef9d0983-533bd4ccd1d84e88af7edf64ef9d0983-0.jpg');
        $resource5->setActivatedAt(new \DateTimeImmutable('2022-01-01 00:00:00'));

        $manager->persist($resource5);
        $manager->flush();

        /*
         *Resource 6
         */

        $resource6= new Resource();
        $resourceType6= new ResourceType();
        $resourceType6->setLabel('Atelier');
        $category6= new Category();
        $category6->setLabel('Autre');
        $resource6->setType($resourceType6);
        $resource6->setCategory($category6);
        $resource6->setName('The Pragmatic Programmer');
        $resource6->setDescription('Straight from the programming trenches, The Pragmatic Programmer cuts through the increasing specialization and technicalities of modern software development to examine .');
        $resource6->setImage('https://media-exp1.licdn.com/dms/image/C4D12AQEy8EiHvkUP7w/article-cover_image-shrink_720_1280/0/1628087337780?e=1648684800&v=beta&t=fINMSPLzLMjbMvNZS5rDqppuMmnkTMdazx7tKCKH1Hs');
        $resource6->setActivatedAt(new \DateTimeImmutable('2022-01-01 00:00:00'));

        $manager->persist($resource6);
        $manager->flush();

        /*
         *Resource 7
         */

        $resource7= new Resource();
        $category7= new Category();
        $resourceType7= new ResourceType();
        $resourceType7->setLabel('Autre');
        $category7->setLabel('Autre');
        $resource7->setType($resourceType7);
        $resource7->setCategory($category7);
        $resource7->setName('1962 : l\'An 2000 vu par les jeunes | Archive INA');
        $resource7->setDescription('Vidéo d\'archive de l\'Institut National de l\'Audiovisuel réalisée le 3 octobre 1962.');
        $resource7->setImage('https://i.ytimg.com/vi/kAMQZ5N49WU/maxresdefault.jpg');
        $resource7->setActivatedAt(new \DateTimeImmutable('2022-01-01 00:00:00'));

        $manager->persist($resource7);
        $manager->flush();

        /*
         *Resource 8
         */

        $resource8= new Resource();
        $resourceType8= new ResourceType();
        $resourceType8->setLabel('Vidéo');
        $category8= new Category();
        $category8->setLabel('Autre');
        $resource8->setType($resourceType8);
        $resource8->setCategory($category8);
        $resource8->setName('Les Innovateurs - Walter ISAACSON');
        $resource8->setDescription('L’explosion numérique n’est pas seulement le fait de quelques génies, c’est une entreprise collective, le résultat d’un travail d’équipes qui ont avancé ensemble, bravant les obstacles les uns après les autres.');
        $resource8->setImage('https://www.sowhat-blog.fr/wp-content/uploads/2017/05/Image-%C3%A0-la-une-Les-innovateurs-%E2%80%93-Walter-Isaacson.jpg');
        $resource8->setActivatedAt(new \DateTimeImmutable('2022-01-01 00:00:00'));

        $manager->persist($resource8);
        $manager->flush();

        /*
         *Resource 9
         */

        $resource9= new Resource();
        $resourceType9= new ResourceType();
        $resourceType9->setLabel('Défi');
        $category9= new Category();
        $category9->setLabel('Autre');
        $resource9->setType($resourceType9);
        $resource9->setCategory($category9);
        $resource9->setName('Metting de Melenchon du 16 janvier 2022');
        $resource9->setDescription('Jean-Luc Mélenchon organise dimanche 16 janvier à Nantes un meeting \"immersif\". Après les hologrammes, le candidat de la France Insoumise propose donc un dispositif inédit. Une fois encore La France insoumise veut créer l\'événement.');
        $resource9->setImage('https://www.francetvinfo.fr/pictures/2THik5rrCwp6k9K8vEYs3-SDi3M/752x423/2022/01/17/phpB8GkFT.jpg');
        $resource9->setActivatedAt(new \DateTimeImmutable('2022-01-01 00:00:00'));

        $manager->persist($resource9);
        $manager->flush();

    }
    public function getOrder(): int
    {
        return 3;
    }
}