Pour récupérer ce dossier, tu devras lancer les commandes (attention, il faut que tu sois dans le dossier api) :

    composer install
    yarn install

Créer le fichier .env.local

    php bin/console doctrine:database:create
    php bin/console doctrine:migration:migrate
    php -S localhost:8000 -t public 
