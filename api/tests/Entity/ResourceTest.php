<?php declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Resource;
use App\Entity\ResourceType;
use PHPUnit\Framework\TestCase;

final class ResourceTest extends TestCase
{
    /** @test */
    public function setType()
    {
        $resource = new Resource();
        $resourceType = new ResourceType();
        $resource ->setType($resourceType);

        $this->assertEquals($resourceType, $resource->getType());
    }

    /** @test */
    public function setCategory()
    {
        $resource = new Resource();
        $category = new Category();
        $resource->setCategory($category);

        $this->assertEquals($category, $resource->getCategory());
    }
}