import {Navbar} from "./components/Navbar";
import Image from "next/image";
import {Dialog, Transition} from "@headlessui/react";
import {Fragment, useState} from 'react'
import {Footer} from "./components/Footer";
import Carousel from "./components/Carousel";
import { useMediaQuery } from 'react-responsive';
import Link from 'next/link'

function Resource(props) {
    const [isOpen, setIsOpen] = useState(false)

    const [modalDetailisOpen, setModalDetailsIsOpen] = useState(false)

    const [selectedRessource, setSelectedResource] = useState(props.resources[0])

    function closeModal() {
        setIsOpen(false)
    }

    function openModal() {
        setIsOpen(true)
    }

    function openDetailModal(id) {
        const indexedId = id - 1
        setSelectedResource(props.resources[indexedId])
        setModalDetailsIsOpen(true)
    }

    function closeDetailModal() {
        setModalDetailsIsOpen(false)
    }
    const users = props.users.map((user) => {
        return (
            // eslint-disable-next-line react/jsx-key
            <div className="flex flex-col gap-2 justify-center items-center" key={user.id}>
                <div className="rounded-full h-20 w-20 relative col-span-1 my-4 cursor-pointer" style={{backgroundImage : `url(${user.image})`, backgroundRepeat: 'norepeat', backgroundSize: 'cover' }}>

                </div>
                <p className="text-black">{user.firstName}</p>
            </div>

        )

    });



    const threeFirstResources = props.resources.slice(0,4);
    const resources = threeFirstResources.map((resource) => {
        return (

            <div key={resource.id} style={{padding: 8}}>

                <div
                    className="relative max-w-sm rounded overflow-hidden shadow-lg h-96" >
                    <img className="h-40 lg:h-48 w-full object-cover" src={resource.image} alt="image de la ressource"/>
                    <div id="openDetailModal" className="relative px-6 py-4 cursor-pointer"  onClick={() => (openDetailModal(resource.id) )}>
                        <h4 className="font-semibold text-xl text-dark mb-3 pt-7 truncate">
                            {resource.name}
                        </h4>
                        <p className="truncate">
                            {resource.description}
                        </p>
                        <span
                            className="mt-4 inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#{resource.type.label}</span>
                    </div>

                    <button
                        name="partager"
                        type="button"
                        data-cy="submit"
                        onClick={openModal}
                        style={{backgroundColor: "#03989E"}}
                        className="absolute bottom-5 right-5 flex px-4 py-2 text-sm font-medium text-white rounded-md bg-opacity-20 hover:bg-opacity-30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75"
                    >
                        <p>Partager</p>
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24"
                             stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 5l7 7-7 7"/>
                        </svg>
                    </button>
                </div>

                <Transition appear show={modalDetailisOpen} as={Fragment}>
                    <Dialog
                        as="div"
                        className="fixed inset-0 z-10 overflow-y-auto"
                        onClose={closeDetailModal}
                    >
                        <div className="min-h-screen px-4 text-center">
                            <Transition.Child
                                as={Fragment}
                                enter="ease-out duration-300"
                                enterFrom="opacity-0"
                                enterTo="opacity-100"
                                leave="ease-in duration-200"
                                leaveFrom="opacity-100"
                                leaveTo="opacity-0"
                            >
                                <Dialog.Overlay className="fixed inset-0"/>
                            </Transition.Child>

                            {/* This element is to trick the browser into centering the modal contents. */}
                            <span
                                className="inline-block h-screen align-middle"
                                aria-hidden="true"
                            >
              &#8203;
            </span>
                            <Transition.Child
                                as={Fragment}
                                enter="ease-out duration-300"
                                enterFrom="opacity-0 scale-95"
                                enterTo="opacity-100 scale-100"
                                leave="ease-in duration-200"
                                leaveFrom="opacity-100 scale-100"
                                leaveTo="opacity-0 scale-95"
                            >

                                <div
                                    className="inline-block w-full max-w-2xl p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl relative">
                                    <Dialog.Title
                                        as="h3"
                                        className="text-lg font-medium leading-6 text-gray-900"
                                    >
                                        <p onClick={closeDetailModal}
                                           className="cursor-pointer absolute right-5 top-5">X</p>
                                        Voir le détail de la ressource
                                    </Dialog.Title>
                                    <div className="mt-2">
                                        <img className="object-contain" src={selectedRessource.image} alt="image de la ressource"/>
                                        <br/>
                                        <p className="font-bold">
                                           Titre : {selectedRessource.name}
                                        </p>
                                        <br/>
                                        <p>Description : {selectedRessource.description}
                                            </p>
                                        <p>Catégorie : <span className="mt-4 inline-block bg-teal-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#{selectedRessource.category.label}</span>



                                            </p>
                                        <p>
                                            Type :  <span className="mt-4 inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#{selectedRessource.type.label}</span>

                                        </p>
                                    </div>

                                    <div className="mt-4 flex justify-end">
                                        <Link href={selectedRessource.image}>
                                        <button
                                            type="button"
                                            className="inline-flex justify-center px-4 py-2 text-sm font-bold text-white bg-teal-400 border border-transparent rounded-md hover:bg-teal-600 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
                                        >
                                            Aller à la source
                                        </button>
                                        </Link>
                                    </div>
                                </div>
                            </Transition.Child>
                        </div>
                    </Dialog>
                </Transition>


            </div>


        );
    });
    const isMobile = useMediaQuery({ query: `(max-width: 760px)` });

    return (
        <>
            <div>
                <Navbar/>
                <div className="flex flex-no-wrap">
                    <div className="h-64">
                        <section className="lg:pt-[20px]">
                            <div className="">
                                <div className="flex h-48 w-screen px-5" style={{backgroundColor: "#03989E"}}>
                                    <div className="flex flex-col gap-2 lg:gap-4 justify-center items-start text-left mx-auto w-10/12">
                                        <h1 className="font-bold text-3xl sm:text-4xl md:text-[40px] text-white">
                                            Bienvenue sur (Re)ssources
                                        </h1>
                                        <p className="text-base text-black font-bold">
                                            La plateforme pour améliorer vos relations.
                                        </p>
                                        <a href="#ressources" className="flex">
                                            <button className='rounded-full border-2 border-orange-300 bg-orange-300 hover:bg-orange-400 hover:border-orange-400 py-2 px-3 text-white  font-bold uppercase tracking-wide'>
                                                Voir les ressources
                                            </button>
                                        </a>

                                    </div>
                                </div>
                            </div>

                            <div>
                                <div
                                    className="flex flex-col lg:flex-row flex-wrap my-6 gap-2 justify-center items-center grow w-screen mx-auto my-20">

                                    <div className="flex flew-row gap-4 cursor-pointer">
                                        <Link href='/category1'>
                                        <div id="category1" className="relative h-40 w-40 lg:h-48 lg:w-80 bg-gray-100 rounded-lg overflow-hidden">
                                            <div className="absolute bottom-5 left-5 right-5 flex flex-col items-center justify-end mx-auto h-20 lg:h-48 w-auto lg:w-40">
                                                <Image src="/../public/png/intelligence-emo.png"
                                                       className="rounded-lg object-scale-down"
                                                       layout="fixed"
                                                       height="107"
                                                       width="160"
                                                />
                                            </div>

                                            <p className="font-bold absolute top-0 m-2">Intelligence émotionnelle</p>
                                        </div>
                                        </Link>
                                        {/* eslint-disable-next-line @next/next/no-html-link-for-pages */}
                                        <Link href='/category2'>
                                        <div className="relative h-40 w-40 lg:h-48 lg:w-80 bg-gray-100 rounded-lg overflow-hidden cursor-pointer">
                                            <div className="flex mx-auto h-10 lg:h-20 w-auto lg:w-40">
                                                <Image src="/../public/png/monde-pro.png"
                                                       className="rounded-lg object-contain"
                                                       layout="fixed"
                                                       height="250"
                                                       width="250"
                                                />
                                            </div>
                                            <p className="font-bold absolute top-0 m-2">Monde professionnel</p>
                                        </div>
                                        </Link>
                                    </div>

                                    <div className="flex flew-row gap-4 cursor-pointer">
                                        <Link href='/category3'>
                                        <div className="relative h-40 w-40 lg:h-48 lg:w-80 bg-gray-100 rounded-lg overflow-hidden">
                                            <div className="flex mx-auto h-20 w-40">
                                                <Image src="/../public/png/qualite-vie.png"
                                                       className="rounded-lg object-contain"
                                                       layout="fixed"
                                                       height="250"
                                                       width="250"
                                                />
                                            </div>
                                            <p className="font-bold absolute top-0 m-2">Qualité de vie</p>
                                        </div>
                                        </Link>
                                        <Link href='/category4'>
                                        <div className="relative h-40 w-40 lg:h-48 lg:w-80 bg-gray-100 rounded-lg overflow-hidden">
                                            <div className="absolute bottom-5 left-5 right-5 flex flex-col items-center justify-end mx-auto h-20 lg:h-48 w-auto lg:w-40">
                                                <Image src="/../public/png/dev-personnel.png"
                                                       className="object-contain rounded-lg scale-70"
                                                       layout="fixed"
                                                       height="107"
                                                       width="200"
                                                       alt="Image représentant le développement personnel"
                                                />
                                            </div>
                                            <p className="font-bold absolute top-0 m-2">Développement personnel</p>
                                        </div>
                                        </Link>
                                    </div>
                                </div>

                            </div>

                            <div className="w-screen" id="ressources">
                                <div className="flex flex-col gap-4 w-10/12 mx-auto my-6">
                                    <h2 className="text-black text-2xl uppercase font-bold">Les plus partagées</h2>
                                    <div className="flex flex-nowrap gap-4">
                                        <div className="w-full max-w-screen">
                                            <Carousel show={isMobile ? 1 : 3}>
                                                {resources}
                                            </Carousel>
                                        </div>
                                        <Transition appear show={isOpen} as={Fragment}>
                                            <Dialog
                                                as="div"
                                                className="fixed inset-0 z-10 overflow-y-auto"
                                                onClose={closeModal}
                                            >
                                                <div className="min-h-screen px-4 text-center">
                                                    <Transition.Child
                                                        as={Fragment}
                                                        enter="ease-out duration-300"
                                                        enterFrom="opacity-0"
                                                        enterTo="opacity-100"
                                                        leave="ease-in duration-200"
                                                        leaveFrom="opacity-100"
                                                        leaveTo="opacity-0"
                                                    >
                                                        <Dialog.Overlay className="fixed inset-0"/>
                                                    </Transition.Child>

                                                    {/* This element is to trick the browser into centering the modal contents. */}
                                                    <span
                                                        className="inline-block h-screen align-middle"
                                                        aria-hidden="true"
                                                    >
              &#8203;
            </span>
                                                    <Transition.Child
                                                        as={Fragment}
                                                        enter="ease-out duration-300"
                                                        enterFrom="opacity-0 scale-95"
                                                        enterTo="opacity-100 scale-100"
                                                        leave="ease-in duration-200"
                                                        leaveFrom="opacity-100 scale-100"
                                                        leaveTo="opacity-0 scale-95"
                                                    >

                                                        <div
                                                            className="inline-block w-full max-w-2xl p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl relative">
                                                            <Dialog.Title
                                                                as="h3"
                                                                className="text-lg font-medium leading-6 text-gray-900"
                                                            >
                                                                <p onClick={closeModal}
                                                                   className="cursor-pointer absolute right-5 top-5">X</p>
                                                                Créer un nouveau salon
                                                            </Dialog.Title>
                                                            <div className="mt-2">
                                                                <form className="px-8 pt-6 pb-8 mb-4">
                                                                    <div className="mb-4">
                                                                        <label
                                                                            className="block text-gray-700 text-sm font-bold mb-2"
                                                                            htmlFor="Titre">
                                                                            Titre
                                                                        </label>
                                                                        <input
                                                                            name="title"
                                                                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline placeholder:text-xs"
                                                                            id="title" type="text"
                                                                            placeholder="Par exemple : Vingt Mille Lieues sous les mers - Roman de Jules Verne"/>
                                                                    </div>
                                                                    <div className="mb-4">
                                                                        <label
                                                                            className="block text-gray-700 text-sm font-bold mb-2"
                                                                            htmlFor="Avec qui ?">
                                                                            Avec qui ?
                                                                        </label>
                                                                        <input
                                                                            name="member"
                                                                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                                                            id="member" type="text" placeholder=""/>
                                                                    </div>


                                                                    <div
                                                                        className="border rounded-lg shadow-xs justify-between p-5 grid grid-cols-2 lg:grid-cols-4">


                                                                        {users}


                                                                    </div>

                                                                </form>
                                                            </div>

                                                            <div className="mt-4 flex justify-end">
                                                                <button
                                                                    id="createChatroom"
                                                                    type="button"
                                                                    className="inline-flex justify-center px-4 py-2 text-sm font-bold text-white bg-teal-400 border border-transparent rounded-md hover:bg-teal-600 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
                                                                    onClick={closeModal}
                                                                >
                                                                    Accéder au salon
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </Transition.Child>
                                                </div>
                                            </Dialog>
                                        </Transition>


                                    </div>
                                </div>
                                <Footer />
                            </div>


                        </section>
                    </div>
                </div>
            </div>
        </>)
}


export async function getServerSideProps(context) {
    const [resourcesResponse, usersResponse] = await Promise.all([
        fetch('http://127.0.0.1:8000/api/resources'),
        fetch('http://127.0.0.1:8000/api/users')
    ]);
    const [resources, users] = await Promise.all([
        resourcesResponse.json(),
        usersResponse.json()
    ]);

    return {props: {resources, users}};
}

export default Resource
