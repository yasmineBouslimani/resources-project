function Login() {
    return (
        <div className="row">
            <div className="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                <form className="reg-page">
                    <div className="reg-header">
                        <h2>Se connecter</h2>
                    </div>


                    <div className="alert alert-danger text-center">
                        <strong>E-mail ou mot de passe incorrect</strong>
                    </div>

                    <div className="input-group margin-bottom-20">
                        <span className="input-group-addon"><i className="fa fa-user" /></span>
                        <input type="email" placeholder="Adresse e-mail" className="form-control"
                               name="email" value="email"/>
                    </div>

                    <div className="input-group margin-bottom-20">
                        <span className="input-group-addon"><i className="fa fa-lock" /></span>
                        <input type="password" placeholder="Mot de passe" className="form-control"
                               name="password" value="password" />
                    </div>

                    <div className="row">
                        <div className="col-md-6 checkbox">
                            <label>
                                <input type="checkbox" name="remember" /> Rester connecté;
                            </label>
                        </div>
                        <div className="col-md-6">
                            <button className="btn-u rounded pull-right" type="submit">Se connecter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}
export default Login
