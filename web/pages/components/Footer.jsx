import Image from "next/image";

export const Footer = () => {
    return (
        <>
           <div className="bg-orange-300 w-full flex flex-col lg:flex-row gap-4 lg:gap-0 justify-around items-start p-4 pt-8">
               <div className="flex flex-col h-[7rem] w-20">
                   <p className="uppercase text-md lg:text-lg text-black font-bold flex">Ressources <br/>Relationnelles</p>

                   <Image src="/png/logo-ministere.png" alt={"Logo Ministère"}
                          width={100}
                          height={150}
                   />
               </div>
               <div className="flex flex-col gap-4">
                   <a href="#">Plan du site</a>
                   <a href="#">Mentions légales</a>
                   <a href="#">FAQ</a>
                   <a href="#">Données personnelles et cookies</a>
                   <a href="#">Contacts</a>
                   <a href="#">Aide</a>
               </div>
               <div className="flex flex-col gap-4">
                   <a href="https://solidarites-sante.gouv.fr/" rel="noreferrer" target="_blank">www.solidarites-sante.gouv.fr</a>
                   <a href="#">www.service public.fr</a>
                   <a href="#">www.gouvernement.fr</a>
                   <a href="#">www.france.fr</a>
               </div>
           </div>

        </>
    )
}