import React from "react";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import {Navbar} from "../pages/components/Navbar";

describe("Navbar", () => {

    it("should show the login button", () => {
        render(<Navbar />)
        const button = screen.getByText('Se connecter', {exact:false});
    })

    it("should show the signup button", () => {
        render(<Navbar />)
        const button = screen.getByText('Créer son profil', {exact:false});
    })
})