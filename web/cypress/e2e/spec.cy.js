describe('The Home Page', () => {
  it('successfully loads and opens the create room modal', () => {
    cy.visit('/')
    cy.get('[data-cy="submit"]').contains('Partager').click()
    cy.get('h3').should('contain', 'Créer un nouveau salon')
    cy.get('input[name="title"]')
    cy.get('input[name="member"]')
    cy.get('#createChatroom')
    cy.get('p').contains('X').click()
  })

  it('should test routes for user', () => {
     cy.intercept('GET', '**/users/*').as('getAllUsers')
     cy.intercept('POST', '**/users/*').as('createUser')

     cy.intercept({
       method: '/PUT|PATCH/',
       url: '**/users/*',
     }).as('updateOrCreate')
    })

  it('should go to a page category', ()=>{
    cy.get('#category1').click()
    cy.url().should('include', '/category1')
  })


})


 /*    cy.server()
     cy.route({
       method: 'GET',         *//* Route all GET requests                  *//*
       url: '/user *//*',        *//* that have a URL that matches '/users *//*' *//*
       response: {            *//* and force the response to be: {         *//*
         firstName: 'Prénom0',   *//*   "firstname": "",                  *//*
         lastName: 'Nom0',     *//*   "lastname": ""                     *//*
       },                     *//* }                                       *//*
     }).as('user'); */


/*   it('should fetch all the resources', () => {
      cy.server()
      cy.intercept({
        method: 'GET',
        url: '/resources *//*',
        response: {
          name: 'This is my title',
          description: 'This is my description',
          image: 'https://picsum.photos/200/300'
        },
      }).as('resource');
     }) */



     /*  cy.intercept('/resources').as('fetchResources')
      cy.wait('@fetchResources').its('response.body')
          .then((resources) => {
            cy.get('.carousel-content div')
              .should('have.length', 4)

            resources.forEach((resource) => {
              cy.contains('', resource)
            })
          })*/