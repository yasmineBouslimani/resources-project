describe("Given the resource categories", () => {
  context("When I send GET /category1", () => {
    it("Then it should return the resources with the category1", () => {
      cy.request({
        method: "GET",
        url: "http://localhost:3000/category1",
      }).should((response) => {
        expect(response.status).to.eq(200);
      });
    });
  });
});